package com.course;

import com.course.model.impl.*;
import com.course.service.DiscountService;
import com.course.service.OrderService;
import com.course.service.UserService;
import com.course.service.impl.DiscountServiceImpl;
import com.course.service.impl.ItemServiceImpl;
import com.course.service.impl.OrderServiceImpl;
import com.course.service.impl.UserServiceImpl;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

public class DiscountTest {

    @Test
    public void itemCreation() throws FileNotFoundException {

        Scanner s = new Scanner(new File("production.txt"));
        ArrayList<String> production = new ArrayList<String>();
        while (s.hasNext()) {
            production.add(s.next());
        }
        s.close();

        List<ItemDTO> items = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            ItemDTO item = new ItemDTO();
            item.setName(production.get(i));
            item.setDescription(description.get(new Random().nextInt(9)));
            item.setPrice(getRandomPrice());
            item.setUniqueNumber(UUID.randomUUID().toString());
            items.add(item);
        }
        ItemServiceImpl itemService = new ItemServiceImpl();
        itemService.save(items);
    }

    @Test
    public void discountCreation() {

        DiscountDTO discount1 = new DiscountDTO();
        discount1.setFinalDate(LocalDateTime.now().plusDays(new Random().nextInt(5)));
        discount1.setInterestRate(new BigDecimal(30));
        discount1.setName("big");

        DiscountDTO discount2 = new DiscountDTO();
        discount2.setFinalDate(LocalDateTime.now().plusDays(new Random().nextInt(5)));
        discount2.setInterestRate(new BigDecimal(20));
        discount2.setName("medium");

        DiscountDTO discount3 = new DiscountDTO();
        discount3.setFinalDate(LocalDateTime.now().plusDays(new Random().nextInt(5)));
        discount3.setInterestRate(new BigDecimal(10));
        discount3.setName("small");

        List<DiscountDTO> discounts = Arrays.asList(discount1, discount2, discount3);
        DiscountService discountService = new DiscountServiceImpl();
        discountService.save(discounts);
    }

    @Test
    public void discountAssignment() {
        DiscountService discountService = new DiscountServiceImpl();
        discountService.discountAssignment(400, 500, new BigDecimal(30));
        discountService.discountAssignment(300, 399, new BigDecimal(20));
        discountService.discountAssignment(200, 299, new BigDecimal(10));
    }



    @Test
    public void userCreation() {
        UserDTO u = new UserDTO();
        u.setName("Артишок");
        u.setEmail("qwer@ty.asd");
        u.setPassword("1234");
        u.setSurname("Бывалый");
        UserServiceImpl userService = new UserServiceImpl();
        userService.save(u);
    }

    @Test
    public void provideDiscount() {
        UserService userService = new UserServiceImpl();
        userService.assignDiscountToUser();
    }

    @Test
    public void createOrders() {
        OrderService orderService = new OrderServiceImpl();
        orderService.createOrder(4, 250, 450);
    }

    @Test
    public void showOrders() {
        OrderService orderService = new OrderServiceImpl();
        List<OrderDTO> orderDTOS = orderService.getOrdersInfo();

    }

    private String[] arrayDescription = {
            "описание 1",
            "описание 2",
            "описание 3",
            "описание 4",
            "описание 5",
            "описание 6",
            "описание 7",
            "описание 8",
            "описание 9",
            "описание 10",

    };
    private List<String> description = new ArrayList<>(Arrays.asList(arrayDescription));

    private BigDecimal getRandomPrice() {
        String range = String.valueOf(new Random().nextInt((500 - 100) + 1) + 100);
        return new BigDecimal(range + ".0");
    }

    public static int randomNum(int min, int max) { //max & min values included
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }


}



