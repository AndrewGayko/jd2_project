package com.course.model.impl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class DiscountDTO {
    private Long id;

    private String name;

    private BigDecimal interestRate;

    private LocalDateTime finalDate;

    private List<ItemDTO> items = new ArrayList<>();

   // private List<UserDTO> users;

    public DiscountDTO() {
    }

    public DiscountDTO(Long id, String name, BigDecimal interestRate, LocalDateTime finalDate, List<ItemDTO> items/*, List<UserDTO> users*/) {
        this.id = id;
        this.name = name;
        this.interestRate = interestRate;
        this.finalDate = finalDate;
        this.items = items;
       // this.users = users;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public LocalDateTime getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(LocalDateTime finalDate) {
        this.finalDate = finalDate;
    }

    public List<ItemDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemDTO> items) {
        this.items = items;
    }

    /*public List<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UserDTO> users) {
        this.users = users;
    }*/
}
