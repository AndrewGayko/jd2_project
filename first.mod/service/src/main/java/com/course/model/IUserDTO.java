package com.course.model;

import com.course.converters.impl.DTOConverter;
import com.course.model.impl.UserDTO;
import com.course.model.User;

public interface IUserDTO extends DTOConverter<User, UserDTO> {
}
