package com.course.model.impl;


import java.util.List;

public class UserDTO {

    private Long id;

    private String name;

    private String email;

    private String surname;

    private String password;

    private RoleDTO role;

    private List<OrderDTO> items;

    private DiscountDTO discountDTO;

    public UserDTO() {
    }

    public UserDTO(Long id, String name, String email, String surname, String password, RoleDTO role, List<OrderDTO> items, DiscountDTO discountDTO) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.surname = surname;
        this.password = password;
        this.role = role;
        this.items = items;
        this.discountDTO = discountDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleDTO getRole() {
        return role;
    }

    public void setRole(RoleDTO role) {
        this.role = role;
    }

    public List<OrderDTO> getItems() {
        return items;
    }

    public void setItems(List<OrderDTO> items) {
        this.items = items;
    }

    public DiscountDTO getDiscountDTO() {
        return discountDTO;
    }

    public void setDiscountDTO(DiscountDTO discountDTO) {
        this.discountDTO = discountDTO;
    }
}
