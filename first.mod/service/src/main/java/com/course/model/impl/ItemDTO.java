package com.course.model.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ItemDTO {
    private Long id;

    private String name;

    private String description;

    private String uniqueNumber;

    private BigDecimal price;

    private List<OrderDTO> users = new ArrayList<>();

    private List<DiscountDTO> discountsDTO = new ArrayList<>();

    public ItemDTO() {
    }

    public ItemDTO(Long id, String name, String description, String uniqueNumber, BigDecimal price, List<OrderDTO> users, List<DiscountDTO> discountsDTO) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.uniqueNumber = uniqueNumber;
        this.price = price;
        this.users = users;
        this.discountsDTO = discountsDTO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUniqueNumber() {
        return uniqueNumber;
    }

    public void setUniqueNumber(String uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public List<OrderDTO> getUsers() {
        return users;
    }

    public void setUsers(List<OrderDTO> users) {
        this.users = users;
    }

    public List<DiscountDTO> getDiscountsDTO() {
        return discountsDTO;
    }

    public void setDiscountsDTO(List<DiscountDTO> discountsDTO) {
        this.discountsDTO = discountsDTO;
    }
}