package com.course.service.impl;

import com.course.converters.impl.entity.UserConverter;
import com.course.converters.impl.dto.UserDTOConverter;
import com.course.dao.DiscountDao;
import com.course.dao.impl.DiscountDaoImpl;
import com.course.model.impl.UserDTO;
import com.course.service.UserService;
import com.course.model.Discount;

import com.course.dao.UserDaoNew;
import com.course.dao.impl.UserDaoImplNew;
import com.course.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class UserServiceImpl implements UserService {

    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

    private UserDaoNew userDao = new UserDaoImplNew(User.class);
    private UserDTOConverter userDTOConverter = new UserDTOConverter();
    private UserConverter userConverter = new UserConverter();

    @Override
    public UserDTO save(UserDTO user) {
        Session session = userDao.getCurrentSession();
        try {
            Transaction transaction = session.getTransaction();
            if(!transaction.isActive()){
                session.beginTransaction();
            }
            User savedUser = userConverter.toEntity(user);
            userDao.create(savedUser);
            UserDTO userDTO = userDTOConverter.toDTO(savedUser);
            transaction.commit();
            return userDTO;

        } catch (Exception e) {
                if (session.getTransaction().isActive()) {
                    session.getTransaction().rollback();
                }
            logger.error("Failed to save user", e);
            }
        return null;
    }

    @Override
    public List<UserDTO> findAll() {
        Session session = userDao.getCurrentSession();
        try {
            Transaction transaction = session.getTransaction();
            if(!transaction.isActive()){
                session.beginTransaction();
            }
            List<User> users = userDao.findAll();
            List<UserDTO> usersDTO = userDTOConverter.toDTOList(users);
            transaction.commit();
            return usersDTO;
        } catch (Exception e) {
            if (session.getTransaction().isActive()){
                session.getTransaction().rollback();
            }
            logger.error("Failed to find all users", e);
        }
        return Collections.emptyList();
    }

    @Override
    public void assignDiscountToUser() {
        DiscountDao discountDao = new DiscountDaoImpl(Discount.class);
        Session session = userDao.getCurrentSession();
        try {
            Transaction transaction = session.getTransaction();
            if (!transaction.isActive()) {
                session.beginTransaction();
            }
            User user = userDao.findById(1L);
            Discount ds = discountDao.findById(1L);
            user.setDiscount(ds);
            session.flush();

            transaction.commit();

        } catch (Exception e) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
            logger.error("Failed to assign discount to user", e);
        }
    }
}


