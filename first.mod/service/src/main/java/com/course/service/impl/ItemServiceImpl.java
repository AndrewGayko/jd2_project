package com.course.service.impl;

import com.course.converters.impl.entity.ItemConverter;
import com.course.converters.impl.dto.ItemDTOConverter;
import com.course.dao.ItemDao;
import com.course.model.impl.ItemDTO;
import com.course.service.ItemService;
import com.course.dao.impl.ItemDaoImpl;
import com.course.model.Item;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ItemServiceImpl implements ItemService {

    private static final Logger logger = LogManager.getLogger(ItemServiceImpl.class);

    private ItemDao itemDao = new ItemDaoImpl(Item.class);
    private ItemConverter itemConverter = new ItemConverter();
    private ItemDTOConverter itemDTOConverter = new ItemDTOConverter();


    @Override
    public List<ItemDTO> save(List<ItemDTO> itemList) {
        Session session = itemDao.getCurrentSession();
        try {
            Transaction transaction = session.getTransaction();
            if (!transaction.isActive()) {
                session.beginTransaction();
            }
            List<Item> savedItems = new ArrayList<>();
            for (ItemDTO itemDTO : itemList) {
                Item item = itemConverter.toEntity(itemDTO);
                itemDao.create(item);
                savedItems.add(item);
            }
            List<ItemDTO> listItemDTO = itemDTOConverter.toDTOList(savedItems);
            transaction.commit();

            return listItemDTO;
        } catch (Exception e) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
            logger.error("Failed to save item", e);
        }
        return Collections.emptyList();
    }

    @Override
    public List<ItemDTO> findItemsInPriceRange(int from, int to) {
        Session session = itemDao.getCurrentSession();
        try {
            Transaction transaction = session.getTransaction();
            if (!transaction.isActive()) {
                session.beginTransaction();
            }
            List<Item> savedItems = itemDao.findItems(from, to);

            List<ItemDTO> listItemDTo = itemDTOConverter.toDTOList(savedItems);
            transaction.commit();

            return listItemDTo;
        } catch (Exception e) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
            logger.error("Failed to find items", e);
        }
        return Collections.emptyList();
    }

    @Override
    public ItemDTO findById(ItemDTO itemDTO) {
        Session session = itemDao.getCurrentSession();
        Item item = null;
        try {
            Transaction transaction = session.getTransaction();
            if (!transaction.isActive()) {
                session.beginTransaction();
            }
            item = itemDao.findOne(itemDTO.getId());
            transaction.commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
            logger.error("Failed to find item", e);
        }
        if (item != null) {
            return itemDTOConverter.toDTO(item);
        }
        return null;
    }

    @Override
    public List<ItemDTO> findAll() {
        return null;
    }
}




