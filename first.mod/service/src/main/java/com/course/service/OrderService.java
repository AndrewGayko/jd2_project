package com.course.service;

import com.course.model.impl.OrderDTO;

import java.util.List;

public interface OrderService {

    void createOrder(int amt, int min, int max);

    List<OrderDTO> getOrdersInfo();
}
