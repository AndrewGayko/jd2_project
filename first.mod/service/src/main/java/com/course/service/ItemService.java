package com.course.service;

import com.course.model.impl.ItemDTO;

import java.util.List;

public interface ItemService {

    List<ItemDTO> save(List<ItemDTO> items);

    List<ItemDTO> findAll();

    List<ItemDTO> findItemsInPriceRange(int from, int to);

    ItemDTO findById(ItemDTO itemDTO);
}
