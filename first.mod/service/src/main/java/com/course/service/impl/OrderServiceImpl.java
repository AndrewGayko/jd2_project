package com.course.service.impl;

import com.course.dao.ItemDao;
import com.course.dao.UserDaoNew;
import com.course.dao.impl.OrderDaoImpl;
import com.course.dao.impl.UserDaoImplNew;
import com.course.model.Item;
import com.course.model.Order;
import com.course.model.Discount;
import com.course.model.impl.OrderDTO;
import com.course.model.User;
import com.course.model.UserItemId;
import com.course.dao.OrderDao;
import com.course.dao.impl.ItemDaoImpl;
import com.course.service.OrderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class OrderServiceImpl implements OrderService {

    private static final Logger logger = LogManager.getLogger(OrderServiceImpl.class);

    ItemDao itemDao = new ItemDaoImpl(Item.class);
    UserDaoNew userDao = new UserDaoImplNew(User.class);
    OrderDao orderDao = new OrderDaoImpl(Order.class);


    @Override
    public void createOrder(int amt, int min, int max) {
        Session session = itemDao.getCurrentSession();
        try {
            Transaction transaction = session.getTransaction();
            if (!transaction.isActive()) {
                session.beginTransaction();
            }
            User user = userDao.findById(1L);
            List<Item> itemList = itemDao.findItems(min, max);

            Long quantity = itemDao.countItems(min, max);
            Order order = null;
            for (int i = 0; i < amt; i++) {
                Item item = itemList.get(new Random().nextInt(itemList.size()));
                order = new Order();
                order.setCreated(LocalDateTime.now());
                order.setQuantity(Math.toIntExact(quantity));
                order.setId(new UserItemId(item.getId(), user.getId()));
                order.setItem(item);
                order.setUser(user);
                orderDao.create(order);
            }
            transaction.commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
            logger.error("Failed to create order", e);
        }
    }

    @Override
    public List<OrderDTO> getOrdersInfo() {
        Session session = itemDao.getCurrentSession();
          try {
        Transaction transaction = session.getTransaction();
        if (!transaction.isActive()) {
            session.beginTransaction();
        }
        List<Order> orders = orderDao.find();
        for(Order order : orders) {
            logger.info(order.getUser().getName() + " " + order.getUser().getSurname() +
                    "|" + order.getItem().getName() + "|"
                    + order.getQuantity() + "|" + order.getItem().getPrice()
                    + "|" + order.getItem().getPrice().subtract((order.getItem().getPrice().multiply(collectDiscounts(order.getItem().getDiscounts()).add(order.getUser().getDiscount().getInterestRate())).divide(new BigDecimal(100)))));
        }
        transaction.commit();

        } catch (Exception e) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
    }  return Collections.emptyList();

    }

    private BigDecimal collectDiscounts(List<Discount> discounts) {
        BigDecimal totaldiscount = new BigDecimal(0);
        for (Discount d : discounts) {
            totaldiscount = totaldiscount.add(d.getInterestRate());
        }
        return totaldiscount;
    }
}




