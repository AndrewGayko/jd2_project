package com.course.service.impl;

import com.course.converters.impl.entity.DiscountConverter;
import com.course.converters.impl.dto.DiscountDTOConverter;
import com.course.dao.DiscountDao;
import com.course.dao.ItemDao;
import com.course.dao.impl.ItemDaoImpl;
import com.course.model.impl.DiscountDTO;
import com.course.service.DiscountService;
import com.course.dao.impl.DiscountDaoImpl;
import com.course.model.Discount;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.course.model.Item;


import java.math.BigDecimal;
import java.util.*;

public class DiscountServiceImpl implements DiscountService {
    private static final Logger logger = LogManager.getLogger(DiscountServiceImpl.class);

    private DiscountDao discountDao = new DiscountDaoImpl(Discount.class);
    private ItemDao itemDao = new ItemDaoImpl(Item.class);
    private DiscountConverter discountConverter = new DiscountConverter();
    private DiscountDTOConverter discountDTOConverter = new DiscountDTOConverter();

    @Override
    public List<DiscountDTO> save(List<DiscountDTO> discountList) {
        Session session = discountDao.getCurrentSession();
        try {
            Transaction transaction = session.getTransaction();
            if (!transaction.isActive()) {
                session.beginTransaction();
            }
            List<Discount> savedItems = new ArrayList<>();
            for (DiscountDTO discountDTO : discountList) {
                Discount discount = discountConverter.toEntity(discountDTO);
                discountDao.create(discount);
                savedItems.add(discount);
            }
            List<DiscountDTO> listDiscountDTO = discountDTOConverter.toDTOList(savedItems);
            transaction.commit();

            return listDiscountDTO;
        } catch (Exception e) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
            logger.error("Failed to save discount", e);
        }
        return Collections.emptyList();
    }

    @Override
    public List<DiscountDTO> getByInterestRate(BigDecimal percent) {
        Session session = discountDao.getCurrentSession();
        try {
            Transaction transaction = session.getTransaction();
            if (!transaction.isActive()) {
                session.beginTransaction();
            }
            List<Discount> discounts = discountDao.findByInterestRate(percent);
            List<DiscountDTO> discountsDTO = discountDTOConverter.toDTOList(discounts);
            transaction.commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
            logger.error("Failed to find discount", e);
        }
        return Collections.emptyList();
    }

    @Override
    public List<DiscountDTO> findAllItems() {
        return Collections.emptyList();
    }

    @Override
    public void discountAssignment(int min, int max, BigDecimal percent) { //Assign discount to items in price range from min - to max
        Session session = discountDao.getCurrentSession();
        try {
            Transaction transaction = session.getTransaction();
            if (!transaction.isActive()) {
                session.beginTransaction();
            }
            List<Item> items = itemDao.findItems(min, max);
            List<Discount> discounts = discountDao.findByInterestRate(percent);
            List<Discount> discountList = new ArrayList<>(discounts);
            for (Item item : items) {
                item.setDiscounts(discountList);
            }
            List<Item> itemList = new ArrayList<>(items);
            for (Discount discount : discounts) {
                discount.setItems(itemList);
            }
            transaction.commit();
        } catch (Exception e) {
            if (session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
            logger.error("Failed to assign discount", e);
        }
    }
}