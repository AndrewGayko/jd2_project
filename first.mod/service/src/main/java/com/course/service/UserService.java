package com.course.service;

import com.course.model.impl.UserDTO;

import java.util.List;

public interface UserService {

    UserDTO save(UserDTO user);

    //User findUserByEmail(String email);

    List<UserDTO> findAll();

    void assignDiscountToUser();
}
