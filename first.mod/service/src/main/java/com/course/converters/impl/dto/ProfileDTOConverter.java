package com.course.converters.impl.dto;

import com.course.converters.impl.DTOConverter;
import com.course.model.impl.ProfileDTO;
import com.course.model.Profile;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class ProfileDTOConverter implements DTOConverter<Profile, ProfileDTO> {

    @Override
    public ProfileDTO toDTO(Profile entity) {

        UserDTOConverter userDTOConverter = new UserDTOConverter();

        if (entity == null) {
            return null;
        }
        ProfileDTO profileDTO = new ProfileDTO();
        profileDTO.setId(entity.getId());
        profileDTO.setAddress(entity.getAddress());
        profileDTO.setPhone(entity.getPhone());
        profileDTO.setUserDTO(userDTOConverter.toDTO(entity.getUser()));
        return profileDTO;
    }

    @Override
    public List<ProfileDTO> toDTOList(List<Profile> list) {
        return Collections.emptyList();
    }

    @Override
    public Set<ProfileDTO> toDTOSet(Set<Profile> set) {
        return Collections.emptySet();
    }
}
