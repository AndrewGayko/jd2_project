package com.course.converters.impl.entity;

import com.course.converters.impl.Converter;
import com.course.model.impl.ProfileDTO;
import com.course.model.Profile;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class ProfileConverter implements Converter<ProfileDTO, Profile> {

    @Override
    public Profile toEntity(ProfileDTO dto) {

        UserConverter userConverter = new UserConverter();

        if (dto == null) {
            return null;
        }
        Profile profile = new Profile();
        profile.setId(dto.getId());
        profile.setAddress(dto.getAddress());
        profile.setPhone(dto.getPhone());
        profile.setUser(userConverter.toEntity(dto.getUserDTO()));
        return profile;
    }

    @Override
    public List<Profile> toEntityList(List<ProfileDTO> list) {
        return Collections.emptyList();
    }

    @Override
    public Set<Profile> toEntitySet(Set<ProfileDTO> set) {
        return Collections.emptySet();
    }


}
