package com.course.converters.impl.dto;

import com.course.converters.impl.DTOConverter;
import com.course.model.impl.RoleDTO;
import com.course.model.Role;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class RoleDTOConverter implements DTOConverter<Role, RoleDTO> {


    @Override
    public RoleDTO toDTO(Role entity){

        PermissionDTOConverter permissionDTOConverter = new PermissionDTOConverter();

        if (entity==null){
            return null;
        }
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(entity.getId());
        roleDTO.setName(entity.getName());
        roleDTO.setPermissionsDTO(permissionDTOConverter.toDTOSet(entity.getPermissions()));
        return roleDTO;
    }

    @Override
    public List<RoleDTO> toDTOList(List<Role> list) {
        return Collections.emptyList();
    }

    @Override
    public Set<RoleDTO> toDTOSet(Set<Role> set) {

        PermissionDTOConverter permissionDTOConverter = new PermissionDTOConverter();

        return set.stream()
                .map(entity -> new RoleDTO(entity.getId(), entity.getName(), permissionDTOConverter.toDTOSet(entity.getPermissions())))
                .collect(Collectors.toSet());
    }
}
