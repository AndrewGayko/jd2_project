package com.course.converters.impl.dto;


import com.course.converters.impl.DTOConverter;
import com.course.model.impl.DiscountDTO;
import com.course.model.Discount;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DiscountDTOConverter implements DTOConverter<Discount, DiscountDTO> {

    @Override
    public DiscountDTO toDTO(Discount entity) {

        ItemDTOConverter itemDTOConverter = new ItemDTOConverter();
        UserDTOConverter userDTOConverter = new UserDTOConverter();

        if (entity == null) {
            return null;
        }
        DiscountDTO discountDTO = new DiscountDTO();
        discountDTO.setName(entity.getName());
        discountDTO.setInterestRate(entity.getInterestRate());
        discountDTO.setFinalDate(entity.getFinalDate());
        discountDTO.setName(entity.getName());
        discountDTO.setItems(itemDTOConverter.toDTOList(entity.getItems()));
        //discountDTO.setUsers(userDTOConverter.toDTOList(entity.getUsers()));
        return discountDTO;
    }


    @Override
    public List<DiscountDTO> toDTOList(List<Discount> list) {

        ItemDTOConverter itemDTOConverter = new ItemDTOConverter();
        UserDTOConverter userDTOConverter = new UserDTOConverter();

        return list.stream()
                .map(
                        s -> new DiscountDTO(s.getId(), s.getName(), s.getInterestRate(), s.getFinalDate(),
                                itemDTOConverter.toDTOList(s.getItems())/*, userDTOConverter.toDTOList(s.getUsers())*/))
                .collect(Collectors.toList());
    }


    @Override
    public Set<DiscountDTO> toDTOSet(Set<Discount> set) {
        return Collections.emptySet();
    }
}

