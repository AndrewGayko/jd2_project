package com.course.converters.impl.dto;

import com.course.converters.impl.DTOConverter;
import com.course.model.impl.NewsDTO;
import com.course.model.News;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class NewsDTOConverter implements DTOConverter<News, NewsDTO> {

    @Override
    public NewsDTO toDTO(News entity) {

        UserDTOConverter userDTOConverter = new UserDTOConverter();

        if (entity == null) {
            return null;
        }
        NewsDTO newsDTO = new NewsDTO();
        newsDTO.setId(entity.getId());
        newsDTO.setTitle(entity.getTitle());
        newsDTO.setContent(entity.getContent());
        newsDTO.setCreated(entity.getCreated());
        newsDTO.setUserDTO(userDTOConverter.toDTO(entity.getUser()));
        return newsDTO;
    }

    @Override
    public List<NewsDTO> toDTOList(List<News> list) {
        return Collections.emptyList();
    }

    @Override
    public Set<NewsDTO> toDTOSet(Set<News> set) {
        return Collections.emptySet();
    }
}
