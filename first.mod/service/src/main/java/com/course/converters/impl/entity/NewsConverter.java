package com.course.converters.impl.entity;

import com.course.converters.impl.Converter;
import com.course.model.impl.NewsDTO;
import com.course.model.News;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class NewsConverter implements Converter<NewsDTO, News> {

    @Override
    public News toEntity(NewsDTO dto) {

        UserConverter userConverter = new UserConverter();

        if (dto == null) {
            return null;
        }
        News news = new News();
        news.setId(dto.getId());
        news.setTitle(dto.getTitle());
        news.setContent(dto.getContent());
        news.setCreated(dto.getCreated());
        news.setUser(userConverter.toEntity(dto.getUserDTO()));
        return news;
    }

    @Override
    public List<News> toEntityList(List<NewsDTO> list) {
        return Collections.emptyList();
    }

    @Override
    public Set<News> toEntitySet(Set<NewsDTO> set) {
        return Collections.emptySet();
    }


}
