package com.course.converters.impl.entity;

import com.course.converters.impl.Converter;
import com.course.model.impl.RoleDTO;
import com.course.model.Role;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class RoleConverter implements Converter<RoleDTO, Role> {

    @Override
    public Role toEntity(RoleDTO dto) {

        PermissionConverter permissionConverter = new PermissionConverter();

        if (dto == null) {
            return null;
        }
        Role role = new Role();
        role.setId(dto.getId());
        role.setName(dto.getName());
        role.setPermissions(permissionConverter.toEntitySet(dto.getPermissionsDTO()));
        return role;
    }

    @Override
    public List<Role> toEntityList(List<RoleDTO> list) {
        return Collections.emptyList();
    }

    @Override
    public Set<Role> toEntitySet(Set<RoleDTO> set) {

        PermissionConverter permissionConverter = new PermissionConverter();

        return set.stream()
                .map(dto -> new Role(dto.getId(), dto.getName(), permissionConverter.toEntitySet(dto.getPermissionsDTO())))
                .collect(Collectors.toSet());
    }


}
