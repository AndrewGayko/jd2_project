package com.course.converters.impl.dto;

import com.course.converters.impl.DTOConverter;
import com.course.model.impl.OrderDTO;
import com.course.model.Order;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class OrderDTOConverter implements DTOConverter<Order, OrderDTO> {

    @Override
    public OrderDTO toDTO(Order entity) {

        ItemDTOConverter itemDTOConverter = new ItemDTOConverter();
        UserDTOConverter userDTOConverter = new UserDTOConverter();

        if (entity == null) {
            return null;
        }
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId(entity.getId());
        orderDTO.setCreated(entity.getCreated());
        orderDTO.setQuantity(entity.getQuantity());
        orderDTO.setStatus(entity.getStatus());
        orderDTO.setItemDTO(itemDTOConverter.toDTO(entity.getItem()));
        orderDTO.setUserDTO(userDTOConverter.toDTO((entity.getUser())));
        return orderDTO;
    }

    @Override
    public List<OrderDTO> toDTOList(List<Order> list) {
        return Collections.emptyList();
    }

    @Override
    public Set<OrderDTO> toDTOSet(Set<Order> set) {
        return Collections.emptySet();
    }


}