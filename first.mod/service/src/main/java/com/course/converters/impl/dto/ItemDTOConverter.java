package com.course.converters.impl.dto;

import com.course.converters.impl.DTOConverter;
import com.course.model.impl.ItemDTO;
import com.course.model.Item;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ItemDTOConverter implements DTOConverter<Item, ItemDTO> {

    @Override
    public ItemDTO toDTO(Item entity) {

        OrderDTOConverter orderDTOConverter = new OrderDTOConverter();
        DiscountDTOConverter discountDTOConverter = new DiscountDTOConverter();

        if (entity == null) {
            return null;
        }
        ItemDTO itemDTO = new ItemDTO();
        itemDTO.setId(entity.getId());
        itemDTO.setName(entity.getName());
        itemDTO.setDescription(entity.getDescription());
        itemDTO.setPrice(entity.getPrice());
        itemDTO.setUniqueNumber(entity.getUniqueNumber());
        itemDTO.setUsers(orderDTOConverter.toDTOList(entity.getUsers()));
        itemDTO.setDiscountsDTO(discountDTOConverter.toDTOList(entity.getDiscounts()));
        return itemDTO;
    }

    @Override
    public List<ItemDTO> toDTOList(List<Item> list) {

        OrderDTOConverter orderDTOConverter = new OrderDTOConverter();
        DiscountDTOConverter discountDTOConverter = new DiscountDTOConverter();

        return list.stream()
                .map(entity -> new ItemDTO(entity.getId(), entity.getName(), entity.getDescription(), entity.getUniqueNumber(), entity.getPrice(), orderDTOConverter.toDTOList(entity.getUsers()), discountDTOConverter.toDTOList(entity.getDiscounts())))
                .collect(Collectors.toList());

    }

    @Override
    public Set<ItemDTO> toDTOSet(Set<Item> set) {
        return Collections.emptySet();
    }
}
