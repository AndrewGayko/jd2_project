package com.course.converters.impl.dto;

import com.course.converters.impl.DTOConverter;
import com.course.model.impl.UserDTO;
import com.course.model.User;


import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;



public class UserDTOConverter implements DTOConverter<User, UserDTO> {

    @Override
    public UserDTO toDTO(User entity){

        RoleDTOConverter roleDTOConverter = new RoleDTOConverter();
        OrderDTOConverter orderDTOConverter = new OrderDTOConverter();
        DiscountDTOConverter discountDTOConverter = new DiscountDTOConverter();

        if (entity == null) {
            return null;
        }
        UserDTO userDTO = new UserDTO();
        userDTO.setId(entity.getId());
        userDTO.setName(entity.getName());
        userDTO.setEmail(entity.getEmail());
        userDTO.setSurname(entity.getSurname());
        userDTO.setPassword(entity.getPassword());
        userDTO.setRole(roleDTOConverter.toDTO(entity.getRole()));
        userDTO.setItems(orderDTOConverter.toDTOList(entity.getItems()));
        userDTO.setDiscountDTO(discountDTOConverter.toDTO(entity.getDiscount()));
        return userDTO;
    }

    @Override
    public List<UserDTO> toDTOList(List<User> list) {

        RoleDTOConverter roleDTOConverter = new RoleDTOConverter();
        OrderDTOConverter orderDTOConverter = new OrderDTOConverter();
        DiscountDTOConverter discountDTOConverter = new DiscountDTOConverter();

        return list.stream()
                .map(entity -> new UserDTO(entity.getId(), entity.getName(), entity.getEmail(), entity.getSurname(), entity.getPassword(), roleDTOConverter.toDTO(entity.getRole()), orderDTOConverter.toDTOList(entity.getItems()), discountDTOConverter.toDTO(entity.getDiscount())))
                .collect(Collectors.toList());
    }

    @Override
    public Set<UserDTO> toDTOSet(Set<User> set) {
        return Collections.emptySet();
    }


}
