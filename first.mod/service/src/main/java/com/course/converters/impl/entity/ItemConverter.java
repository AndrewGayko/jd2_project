package com.course.converters.impl.entity;

import com.course.converters.impl.Converter;
import com.course.model.impl.ItemDTO;
import com.course.model.Item;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ItemConverter implements Converter<ItemDTO, Item> {

    @Override
    public Item toEntity(ItemDTO dto) {

        OrderConverter orderConverter = new OrderConverter();
        DiscountConverter discountConverter = new DiscountConverter();

        if (dto == null) {
            return null;
        }
        Item item = new Item();
        item.setId(dto.getId());
        item.setName(dto.getName());
        item.setDescription(dto.getDescription());
        item.setPrice(dto.getPrice());
        item.setUniqueNumber(dto.getUniqueNumber());
        item.setUsers(orderConverter.toEntityList(dto.getUsers()));
        item.setDiscounts(discountConverter.toEntityList(dto.getDiscountsDTO()));
        return item;
    }

    @Override
    public List<Item> toEntityList(List<ItemDTO> list) {

        OrderConverter orderConverter = new OrderConverter();
        DiscountConverter discountConverter = new DiscountConverter();

        return list.stream()
                .map(dto -> new Item(dto.getId(), dto.getName(), dto.getDescription(), dto.getUniqueNumber(), dto.getPrice(), orderConverter.toEntityList(dto.getUsers()), discountConverter.toEntityList(dto.getDiscountsDTO())))
                .collect(Collectors.toList());
    }

    @Override
    public Set<Item> toEntitySet(Set<ItemDTO> set) {
        return Collections.emptySet();
    }


}
