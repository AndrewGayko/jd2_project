package com.course.converters.impl.entity;

import com.course.converters.impl.Converter;
import com.course.model.impl.CommentDTO;
import com.course.model.Comment;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class CommentConverter implements Converter<CommentDTO, Comment> {

    @Override
    public Comment toEntity(CommentDTO commentDTO) {

        UserConverter udt = new UserConverter();
        NewsConverter newsConverter = new NewsConverter();

        if (commentDTO == null) {
            return null;
        }
        Comment comment = new Comment();
        comment.setId(commentDTO.getId());
        comment.setContent(commentDTO.getContent());
        comment.setCreated(commentDTO.getCreated());
        comment.setUser(udt.toEntity(commentDTO.getUserDTO()));
        comment.setNews(newsConverter.toEntity(commentDTO.getNewsDTO()));
        return comment;
    }

    @Override
    public List<Comment> toEntityList(List<CommentDTO> list) {
        return Collections.emptyList();
    }

    @Override
    public Set<Comment> toEntitySet(Set<CommentDTO> set) {
        return Collections.emptySet();
    }


}
