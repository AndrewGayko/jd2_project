package com.course.converters.impl.entity;

import com.course.converters.impl.Converter;
import com.course.model.impl.OrderDTO;
import com.course.model.Order;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class OrderConverter implements Converter<OrderDTO, Order> {

    @Override
    public Order toEntity(OrderDTO dto) {

        ItemConverter itemConverter = new ItemConverter();
        UserConverter userConverter = new UserConverter();

        if (dto == null) {
            return null;
        }
        Order order = new Order();
        order.setId(dto.getId());
        order.setQuantity(dto.getQuantity());
        order.setStatus(dto.getStatus());
        order.setCreated(dto.getCreated());
        order.setItem(itemConverter.toEntity(dto.getItemDTO()));
        order.setUser(userConverter.toEntity((dto.getUserDTO())));
        return order;

    }

    @Override
    public List<Order> toEntityList(List<OrderDTO> list) {
        return Collections.emptyList();
    }

    @Override
    public Set<Order> toEntitySet(Set<OrderDTO> set) {
        return Collections.emptySet();
    }


}
