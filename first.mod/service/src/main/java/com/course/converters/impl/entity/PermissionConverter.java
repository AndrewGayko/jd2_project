package com.course.converters.impl.entity;

import com.course.converters.impl.Converter;
import com.course.model.impl.PermissionDTO;
import com.course.model.Permission;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PermissionConverter implements Converter<PermissionDTO, Permission> {

    @Override
    public Permission toEntity(PermissionDTO dto) {

        RoleConverter roleConverter = new RoleConverter();

        if (dto == null) {
            return null;
        }
        Permission permission = new Permission();
        permission.setId(dto.getId());
        permission.setName(dto.getName());
        permission.setRoles(roleConverter.toEntitySet(dto.getRolesDTO()));
        return permission;
    }

    @Override
    public List<Permission> toEntityList(List<PermissionDTO> list) {
        return Collections.emptyList();
    }

    @Override
    public Set<Permission> toEntitySet(Set<PermissionDTO> set) {

        RoleConverter roleConverter = new RoleConverter();

        return set.stream()
                .map(dto -> new Permission(dto.getId(), dto.getName(), roleConverter.toEntitySet(dto.getRolesDTO())))
                .collect(Collectors.toSet());
    }


}
