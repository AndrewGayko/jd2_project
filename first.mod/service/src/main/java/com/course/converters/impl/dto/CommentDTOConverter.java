package com.course.converters.impl.dto;


import com.course.converters.impl.DTOConverter;
import com.course.model.impl.CommentDTO;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import com.course.model.Comment;

public class CommentDTOConverter implements DTOConverter<Comment, CommentDTO> {

    @Override
    public CommentDTO toDTO(Comment entity) {

        UserDTOConverter userDTOConverter = new UserDTOConverter();
        NewsDTOConverter newsDTOConverter = new NewsDTOConverter();


        if(entity == null) {
            return null;
        }
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(entity.getId());
        commentDTO.setContent(entity.getContent());
        commentDTO.setCreated(entity.getCreated());
        commentDTO.setUserDTO(userDTOConverter.toDTO(entity.getUser()));
        commentDTO.setNewsDTO(newsDTOConverter.toDTO(entity.getNews()));
        return commentDTO;
    }

    @Override
    public List<CommentDTO> toDTOList(List<Comment> list) {
        return Collections.emptyList();
    }

    @Override
    public Set<CommentDTO> toDTOSet(Set<Comment> set) {
        return Collections.emptySet();
    }
}
