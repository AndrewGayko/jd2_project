package com.course.converters.impl.dto;

import com.course.converters.impl.DTOConverter;
import com.course.model.impl.PermissionDTO;
import com.course.model.Permission;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PermissionDTOConverter implements DTOConverter<Permission, PermissionDTO> {

    @Override
    public PermissionDTO toDTO(Permission entity) {

        RoleDTOConverter roleDTOConverter = new RoleDTOConverter();

        if (entity == null) {
            return null;
        }
        PermissionDTO permissionDTO = new PermissionDTO();
        permissionDTO.setId(entity.getId());
        permissionDTO.setName(entity.getName());
        permissionDTO.setRolesDTO(roleDTOConverter.toDTOSet(entity.getRoles()));
        return permissionDTO;


    }

    @Override
    public List<PermissionDTO> toDTOList(List<Permission> list) {
        return Collections.emptyList();
    }

    @Override
    public Set<PermissionDTO> toDTOSet(Set<Permission> set) {

        RoleDTOConverter roleDTOConverter = new RoleDTOConverter();

        return set.stream()
                .map(entity -> new PermissionDTO(entity.getId(), entity.getName(), roleDTOConverter.toDTOSet(entity.getRoles())))
                .collect(Collectors.toSet());
    }
}
