package com.course.converters.impl.entity;

import com.course.converters.impl.Converter;
import com.course.model.impl.DiscountDTO;
import com.course.model.Discount;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class DiscountConverter implements Converter<DiscountDTO, Discount> {

    @Override
    public Discount toEntity(DiscountDTO dto) {

        ItemConverter itemConverter = new ItemConverter();
        UserConverter userConverter = new UserConverter();

        if (dto == null) {
            return null;
        }
        Discount discount = new Discount();
        discount.setId(dto.getId());
        discount.setName(dto.getName());
        discount.setInterestRate(dto.getInterestRate());
        discount.setFinalDate(dto.getFinalDate());
        discount.setItems(itemConverter.toEntityList(dto.getItems()));
        //discount.setUsers(userConverter.toEntityList(dto.getUsers()));
        return discount;
    }

    @Override
    public List<Discount> toEntityList(List<DiscountDTO> list) {

        ItemConverter itemConverter = new ItemConverter();
        UserConverter userConverter = new UserConverter();

        return list.stream()
                .map(dto -> new Discount(dto.getId(), dto.getName(), dto.getInterestRate(), dto.getFinalDate(), itemConverter.toEntityList(dto.getItems())/*, userConverter.toEntityList(dto.getUsers())*/))
                .collect(Collectors.toList());
    }

    @Override
    public Set<Discount> toEntitySet(Set<DiscountDTO> set) {
        return Collections.emptySet();
    }
}
