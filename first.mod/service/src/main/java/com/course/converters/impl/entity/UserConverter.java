package com.course.converters.impl.entity;

import com.course.converters.impl.Converter;
import com.course.model.impl.UserDTO;
import com.course.model.User;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class UserConverter implements Converter<UserDTO, User> {

    @Override
    public User toEntity(UserDTO dto) {

        RoleConverter roleConverter = new RoleConverter();
        OrderConverter orderConverter = new OrderConverter();
        DiscountConverter discountConverter = new DiscountConverter();

        if (dto == null) {
            return null;
        }
        User user = new User();
        user.setId(dto.getId());
        user.setName(dto.getName());
        user.setEmail(dto.getEmail());
        user.setSurname(dto.getSurname());
        user.setPassword(dto.getPassword());
        user.setRole(roleConverter.toEntity(dto.getRole()));
        user.setItems(orderConverter.toEntityList(dto.getItems()));
        user.setDiscount(discountConverter.toEntity(dto.getDiscountDTO()));
        return user;
    }

    @Override
    public List<User> toEntityList(List<UserDTO> list) {

        RoleConverter roleConverter = new RoleConverter();
        OrderConverter orderConverter = new OrderConverter();
        DiscountConverter discountConverter = new DiscountConverter();

        return list.stream()
                .map(dto -> new User(dto.getId(), dto.getName(), dto.getEmail(), dto.getSurname(), dto.getPassword(), roleConverter.toEntity(dto.getRole()), orderConverter.toEntityList(dto.getItems()), discountConverter.toEntity(dto.getDiscountDTO())))
                .collect(Collectors.toList());
    }

    @Override
    public Set<User> toEntitySet(Set<UserDTO> set) {
        return Collections.emptySet();
    }


}
