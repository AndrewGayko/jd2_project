package com.course.converters.impl.entity;

import com.course.converters.impl.Converter;
import com.course.model.impl.AuditDTO;
import com.course.model.Audit;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class AuditConverter implements Converter<AuditDTO, Audit> {

    @Override
    public Audit toEntity(AuditDTO dto) {

        UserConverter userConverter = new UserConverter();

        if (dto == null) {
            return null;
        }
        Audit audit = new Audit();
        audit.setId(dto.getId());
        audit.setEventType(dto.getEventType());
        audit.setCreated(dto.getCreated());
        audit.setUser(userConverter.toEntity(dto.getUserDTO()));
        return audit;
    }


    @Override
    public List<Audit> toEntityList(List<AuditDTO> list) {

        UserConverter userConverter = new UserConverter();

        return list.stream()
                .map(dto -> new Audit(dto.getId(), dto.getEventType(), dto.getCreated(), userConverter.toEntity(dto.getUserDTO())))
                .collect(Collectors.toList());
    }

    @Override
    public Set<Audit> toEntitySet(Set<AuditDTO> set) {
        return Collections.emptySet();
    }
}
