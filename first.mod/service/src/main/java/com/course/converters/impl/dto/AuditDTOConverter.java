package com.course.converters.impl.dto;

import com.course.converters.impl.DTOConverter;
import com.course.model.impl.AuditDTO;
import com.course.model.Audit;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class AuditDTOConverter implements DTOConverter<Audit, AuditDTO> {

    @Override
    public  AuditDTO toDTO(Audit entity) {

        UserDTOConverter userDTOConverter = new UserDTOConverter();

        if (entity == null) {
            return null;
        }
        AuditDTO auditDTO = new AuditDTO();
        auditDTO.setId(entity.getId());
        auditDTO.setEventType(entity.getEventType());
        auditDTO.setCreated(entity.getCreated());
        auditDTO.setUserDTO(userDTOConverter.toDTO(entity.getUser()));
        return auditDTO;
    }

    @Override
    public List<AuditDTO> toDTOList(List<Audit> list) {
        return Collections.emptyList();
    }

    @Override
    public Set<AuditDTO> toDTOSet(Set<Audit> set) {
        return Collections.emptySet();
    }
}
