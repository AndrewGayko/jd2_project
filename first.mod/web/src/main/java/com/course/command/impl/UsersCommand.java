package com.course.command.impl;


import com.course.service.UserService;
import com.course.command.Command;
import com.course.config.ConfigurationManager;
import com.course.service.impl.UserServiceImpl;
import com.course.model.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class UsersCommand implements Command {

    private UserService userService = new UserServiceImpl();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<User> users = userService.findAll();
        request.setAttribute("users", users);
        return ConfigurationManager.getInstance().getProperty(ConfigurationManager.USERS_PAGE_PATH);

    }
}
