package com.course.impl;

import com.course.dao.ItemDao;
import com.course.dao.OrderDao;
import com.course.dao.UserDaoNew;
import com.course.dao.impl.ItemDaoImpl;
import com.course.dao.impl.OrderDaoImpl;
import com.course.dao.impl.UserDaoImplNew;
import com.course.model.Item;
import com.course.model.User;
import com.course.model.Order;
import com.course.model.UserItemId;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import java.time.LocalDateTime;

public class OrderTest {
    OrderDao orderDao = new OrderDaoImpl(Order.class);
    UserDaoNew userDaoNew = new UserDaoImplNew(User.class);
    ItemDao itemDao = new ItemDaoImpl(Item.class);

    @Test
    public void createTest(){
        Order order = new Order();
        order.setCreated(LocalDateTime.now());
        order.setStatus("in_progress");
        order.setQuantity(2);
        Session session = orderDao.getCurrentSession();
        Transaction transaction = session.getTransaction();
        session.beginTransaction();
        User user = userDaoNew.findById(1L);
        Item item = itemDao.findOne(1L);
        order.setId(new UserItemId(user.getId(),item.getId()));
        order.setUser(user);
        order.setItem(item);
        orderDao.create(order);
        transaction.commit();
        session.close();
    }
}
