package com.course.impl;

import com.course.dao.UserDaoNew;
import com.course.dao.impl.UserDaoImplNew;
import org.junit.Test;
import com.course.model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class UserTest {

    UserDaoNew userDaoNew = new UserDaoImplNew(User.class);
    private long id;

    @Test
    public void CreateTestMethod() {
        User user = new User();
        user.setName("username");
        user.setEmail("usermail@mail.ru");
        user.setSurname("usersurname");
        user.setPassword("1111");
        //testuser.setRole.;
        Session session = userDaoNew.getCurrentSession();
        Transaction transaction = session.getTransaction();
        session.beginTransaction();
        userDaoNew.create(user);
        transaction.commit();
        session.close();
    }

    @Test
    public void UpdateTestMethod(){

        Session session = userDaoNew.getCurrentSession();
        Transaction transaction = session.getTransaction();
        session.beginTransaction();
        User testUser = session.load(User.class, id);
        System.out.println("---------------------" + id + "----------------------");
        testUser.setName("upname");
        testUser.setPassword("2222");
        testUser.setEmail("usermail@tut.by");
        testUser.setSurname("upsurname");
        userDaoNew.update(testUser);
        transaction.commit();
        session.close();
    }

    @Test
    public void DeleteTestMethod(){

        Session session = userDaoNew.getCurrentSession();
        Transaction transaction = session.getTransaction();
        session.beginTransaction();
        User testUser = userDaoNew.findOne(id);
        System.out.println("---------------------" + id + "----------------------");
        userDaoNew.delete(testUser);
        transaction.commit();
        session.close();

    }
}