package com.course.impl;

import com.course.dao.NewsDao;
import com.course.dao.UserDaoNew;
import com.course.dao.impl.NewsDaoImpl;
import com.course.dao.impl.UserDaoImplNew;
import com.course.model.News;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;
import com.course.model.User;

import java.time.LocalDateTime;
import java.util.List;

public class NewsTest {
    private NewsDao newsDao = new NewsDaoImpl(News.class);
    private UserDaoNew userDaoNew = new UserDaoImplNew(User.class);

    @Test
    public void createTest(){
        News news = new News();
        news.setContent("test_content");
        news.setCreated(LocalDateTime.now());
        news.setTitle("test_title");
        Session session = newsDao.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        User user = userDaoNew.findById(1L);
        news.setUser(user);
        newsDao.create(news);
        transaction.commit();
        session.close();
    }

    @Test
    public void findTest (){
        Session session = newsDao.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        List<News> news = newsDao.findByUserId(1L);
        System.out.println(news);
        transaction.commit();
        session.close();
    }
}
