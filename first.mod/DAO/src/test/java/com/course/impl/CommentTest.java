package com.course.impl;

import com.course.dao.CommentDao;
import com.course.dao.NewsDao;
import com.course.dao.UserDaoNew;
import com.course.dao.impl.CommentDaoImpl;
import com.course.dao.impl.NewsDaoImpl;
import com.course.dao.impl.UserDaoImplNew;
import com.course.model.Comment;
import com.course.model.News;
import com.course.model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import java.time.LocalDateTime;

public class CommentTest {
    private CommentDao commentDao = new CommentDaoImpl(Comment.class);
    private UserDaoNew userDaoNew = new UserDaoImplNew(User.class);
    private NewsDao newsDao = new NewsDaoImpl(News.class);

    @Test
    public void createTest(){

        Comment comment = new Comment();
        comment.setContent("Some content");
        comment.setCreated(LocalDateTime.now());
        Session session = commentDao.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        User user = userDaoNew.findById(1L);
        comment.setUser(user);
        News news = newsDao.findOne(1L);
        comment.setNews(news);
        commentDao.create(comment);
        transaction.commit();
        session.close();
    }
}
