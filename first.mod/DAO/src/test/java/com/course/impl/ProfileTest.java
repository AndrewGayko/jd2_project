package com.course.impl;

import com.course.dao.ProfileDao;
import com.course.dao.UserDaoNew;
import com.course.dao.impl.ProfileDaoImpl;
import com.course.dao.impl.UserDaoImplNew;
import com.course.model.Profile;
import com.course.model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

public class ProfileTest {

    private ProfileDao profileDao = new ProfileDaoImpl(Profile.class);
    private UserDaoNew userDaoNew = new UserDaoImplNew(User.class);

    @Test
    public void createProfile() {
        Profile profile = new Profile();
        profile.setAddress("some_box");
        profile.setPhone("1234567");
        Session session = profileDao.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        User user = userDaoNew.findById(1L);
        profile.setUser(user);
        transaction.commit();
        session.close();
    }
}
