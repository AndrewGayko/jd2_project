package com.course.impl;

import com.course.dao.PermissionDao;
import com.course.dao.RoleDao;
import com.course.dao.UserDaoNew;
import com.course.dao.impl.PermissionDaoImpl;
import com.course.dao.impl.RoleDaoImpl;
import com.course.dao.impl.UserDaoImplNew;
import com.course.model.Permission;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import com.course.model.User;
import com.course.model.Role;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.HashSet;
import java.util.Set;


public class UserRoleTest {
    private static final Logger logger = LogManager.getLogger(UserRoleTest.class);

    UserDaoNew userDaoNew = new UserDaoImplNew(User.class);
    private User testuser = new User();
    private RoleDao roleDao = new RoleDaoImpl(Role.class);
    private PermissionDao permissionDao = new PermissionDaoImpl(Permission.class);

    //private Role role = new Role();

    @Test
    public void createTest() {
        Role role = new Role();
        Permission permission1 = new Permission();
        Permission permission2 = new Permission();
        Permission permission3 = new Permission();
        role.setName("default");
        permission1.setName("login");
        permission2.setName("registration");
        permission3.setName("logout");
        Set<Role> roles = new HashSet<>();
        roles.add(role);
        Set<Permission> permissions = new HashSet<>();
        permissions.add(permission1);
        permissions.add(permission2);
        permissions.add(permission3);
        permission1.setRoles(roles);
        permission2.setRoles(roles);
        permission3.setRoles(roles);
        role.setPermissions(permissions);
        Session session = userDaoNew.getCurrentSession();
        Transaction transaction = session.getTransaction();
        session.beginTransaction();
        roleDao.create(role);
        transaction.commit();
        session.close();

    }

    @Test
    public void createTest2() {
        Permission permission1 = new Permission();
        Permission permission2 = new Permission();
        permission1.setName("perm1");
        permission2.setName("perm2");
        Role role = new Role();
        role.setName("роль");
        role.addPermission(permission1);
        role.addPermission(permission2);
        Session session = userDaoNew.getCurrentSession();
        Transaction tx = session.getTransaction();
        tx.begin();
        roleDao.create(role);
        tx.commit();
        long id = role.getId();
        System.out.println(id);
        //   assertTrue(id != 0);

    }

    @Test
    public void deleteAllRoles() { //Unbound roles from permissions & unbound permissions from roles

        Transaction tx = null;
        try (Session session = userDaoNew.getCurrentSession()) {
            tx = session.beginTransaction();
            roleDao.deleteAll();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            logger.error(e.getMessage(), e);
        }
    }

    @Test
    public void deleteRolePermissionDependenciesByRoleName() {
        Permission perm1 = new Permission();
        Permission perm2 = new Permission();
        perm1.setName("perm1");
        perm2.setName("perm2");
        Role role = new Role();
        role.setName("роль");
        role.addPermission(perm1);
        role.addPermission(perm2);
        Transaction transaction = null;
        try (Session session = userDaoNew.getCurrentSession()) {
            transaction = session.beginTransaction();
            roleDao.create(role);
            roleDao.unbind("роль");
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            logger.error(e.getMessage(), e);
        }
    }

/*
    @Test
    public void testUserRole() {
        testuser.setName("1username");
        testuser.setEmail("1usermail@mail.ru");
        testuser.setSurname("1usersurname");
        testuser.setPassword("11111");
        testuser.setRole(role.getName("Security user"));
        Session session = userDaoNew.getCurrentSession();
        Transaction transaction = session.getTransaction();
        session.beginTransaction();
        userDaoNew.create(testuser);
        transaction.commit();
        session.close();
    }*/


    @Test
    public void deleteRolePermission() {
        //Permission permission = new Permission();

        Session session = userDaoNew.getCurrentSession();
        Transaction transaction = session.getTransaction();
        session.beginTransaction();
        Permission permission = permissionDao.findByName("perm1");

        Role role = roleDao.getByName("роль");
        role.removePermission(permission);
        transaction.commit();
        session.close();
    }


    @Test
    public void deleteAllUnboundRoles() {

    }

}
