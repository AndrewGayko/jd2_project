package com.course.impl;

import com.course.dao.ItemDao;
import com.course.dao.impl.ItemDaoImpl;
import com.course.model.Item;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.UUID;

public class ItemTest {
    ItemDao itemDao = new ItemDaoImpl(Item.class);

    @Test
    public void createTest() {
        Session session = itemDao.getCurrentSession();
        Transaction transaction = session.getTransaction();
        session.beginTransaction();

        for (int i = 0; i < 30; i++) {
            Item item = new Item();
            item.setName("item_name");
            item.setDescription("item_description");
            item.setPrice(new BigDecimal(Math.random()));
            item.setUniqueNumber(UUID.randomUUID().toString());
            itemDao.create(item);
        }

        transaction.commit();
        session.close();

    }
}
