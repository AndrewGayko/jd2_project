package com.course.impl;

import com.course.dao.AuditDao;
import com.course.dao.UserDaoNew;
import com.course.dao.impl.AuditDaoImpl;
import com.course.dao.impl.UserDaoImplNew;
import com.course.model.Audit;
import com.course.model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.List;

public class AuditTest {
    private AuditDao auditDao = new AuditDaoImpl(Audit.class);
    private UserDaoNew userDaoNew = new UserDaoImplNew(User.class);

    @Test
    public void createTest(){
        Audit audit = new Audit();
        audit.setCreated(LocalDateTime.now());
        audit.setEventType("New event");
        Session session = auditDao.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        User user = userDaoNew.findById(1L);
        audit.setUser(user);
        auditDao.create(audit);
        transaction.commit();
        session.close();


    }

    @Test
    public void eventFindTest(){
        Session session = auditDao.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        List<Audit> audit = auditDao.findByEvent("New event");
        System.out.println(audit);
        transaction.commit();
        session.close();


    }
}
