package com.course.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "t_item")
public class Item implements Serializable {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "id")
private Long id;

@Column(name = "name")
private String name;

@Column(name = "description")
private String description;

@Column(name = "unique_number", columnDefinition = "VARCHAR(50)")
private String uniqueNumber;

@Column(name = "price", precision = 20,scale = 3)
private BigDecimal price;

@OneToMany(mappedBy = "item", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Order> users = new ArrayList<>();

    @ManyToMany(mappedBy = "items", cascade = CascadeType.ALL)
    /*@JoinTable(name = "t_discount_item",
            joinColumns = @JoinColumn(name = "f_item_id"),
            inverseJoinColumns = @JoinColumn(name = "f_discount_id"))*/
    private List<Discount> discounts = new ArrayList<>();

    public Item() {
    }

    public Item(Long id, String name, String description, String uniqueNumber, BigDecimal price, List<Order> users, List<Discount> discounts) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.uniqueNumber = uniqueNumber;
        this.price = price;
        this.users = users;
        this.discounts = discounts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUniqueNumber() {
        return uniqueNumber;
    }

    public void setUniqueNumber(String uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public List<Order> getUsers() {
        return users;
    }

    public void setUsers(List<Order> users) {
        this.users = users;
    }

    public List<Discount> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(List<Discount> discounts) {
        this.discounts = discounts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return Objects.equals(id, item.id) &&
                Objects.equals(name, item.name) &&
                Objects.equals(description, item.description) &&
                Objects.equals(uniqueNumber, item.uniqueNumber) &&
                Objects.equals(price, item.price) &&
                Objects.equals(users, item.users) &&
                Objects.equals(discounts, item.discounts);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, description, uniqueNumber, price, users, discounts);
    }

    /*@Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", uniqueNumber=" + uniqueNumber +
                ", price=" + price +
                ", users=" + users +
                ", discounts=" + discounts +
                '}';
    }*/
}
