package com.course.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "t_user")

public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "surname")
    private String surname;


    @Column(name = "password")
    private String password;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "t_role_id", foreignKey = @ForeignKey(name = "f_role_id_user"))
    private Role role;

   /* @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "user")
    private List<Comment> comments = new ArrayList<>();*/

    @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true, mappedBy = "user")
    private List<Order> items = new ArrayList<>();

   /* @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true,mappedBy = "user")
    private List<Audit> audits = new ArrayList<>();

    @OneToOne(fetch = FetchType.LAZY, *//*mappedBy = "user",*//* cascade = CascadeType.ALL)
    private Profile profile;*/

    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(columnDefinition="integer", name = "discount_id", nullable = true)
    private Discount discount;

    public User() {
    }

    public User(Long id, String name, String email, String surname, String password, Role role, List<Order> items, Discount discount) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.surname = surname;
        this.password = password;
        this.role = role;
        this.items = items;
        this.discount = discount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Order> getItems() {
        return items;
    }

    public void setItems(List<Order> items) {
        this.items = items;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }
}
