package com.course.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Discount implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "interest")
    private BigDecimal interestRate;

    @Column(name = "final_date")
    private LocalDateTime finalDate;

  //  @ManyToMany(mappedBy = "discounts", cascade = CascadeType.ALL, fetch = FetchType.LAZY)

    @ManyToMany
    @JoinTable(
            name = "t_item_discount",
            joinColumns = @JoinColumn(name = "f_discount_id", foreignKey=@ForeignKey(name="foreign_key_discount_id")),
            inverseJoinColumns = @JoinColumn(name = "f_item_id", foreignKey=@ForeignKey(name="foreign_key_item_id"))
    )
    private List<Item> items = new ArrayList<>();
/*

    @OneToMany(mappedBy = "discount", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<User> users;
*/

    public Discount() {
    }

    public Discount(Long id, String name, BigDecimal interestRate, LocalDateTime finalDate, List<Item> items/*, List<User> users*/) {
        this.id = id;
        this.name = name;
        this.interestRate = interestRate;
        this.finalDate = finalDate;
        this.items = items;
        //this.users = users;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public LocalDateTime getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(LocalDateTime finalDate) {
        this.finalDate = finalDate;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

   /* public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }*/
}
