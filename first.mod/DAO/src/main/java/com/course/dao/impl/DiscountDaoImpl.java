package com.course.dao.impl;

import com.course.dao.DiscountDao;
import com.course.model.Discount;

import java.math.BigDecimal;
import java.util.List;

public class DiscountDaoImpl extends GenericDaoImpl<Discount> implements DiscountDao {

    public DiscountDaoImpl(Class<Discount> clazz) {
        super(clazz);
    }

    @Override
    public List<Discount> findByInterestRate(BigDecimal interestRate) {
        return (List<Discount>) getCurrentSession().createQuery("from Discount where interestRate=:interestRate")
                .setParameter("interestRate", interestRate)
                .list();
    }


    @Override
    public Discount findById(Long id) {
        return (Discount) getCurrentSession().createQuery("from Discount where id=:id")
                .setParameter("id", id)
                .uniqueResult();
    }
}
