package com.course.dao;

import com.course.dao.GenericDao;
import com.course.model.Comment;

public interface CommentDao extends GenericDao<Comment> {
}
