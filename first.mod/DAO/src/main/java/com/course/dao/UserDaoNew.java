package com.course.dao;

import com.course.model.User;

public interface UserDaoNew extends GenericDao<User> {

    User findByEmail();

    User findById(long id);


}
