package com.course.dao.impl;

import com.course.dao.CommentDao;
import com.course.model.Comment;

public class CommentDaoImpl extends GenericDaoImpl<Comment> implements CommentDao {

    public CommentDaoImpl(Class<Comment> clazz) {
        super(clazz);
    }
}
