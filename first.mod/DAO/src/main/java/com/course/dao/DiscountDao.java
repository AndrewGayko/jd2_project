package com.course.dao;

import com.course.dao.GenericDao;
import com.course.model.Discount;

import java.math.BigDecimal;
import java.util.List;

public interface DiscountDao extends GenericDao<Discount> {

    List<Discount> findByInterestRate(BigDecimal interestRate);

    Discount findById(Long id);
}
