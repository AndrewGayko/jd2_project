package com.course.dao.impl;

import com.course.dao.ItemDao;
import com.course.model.Item;

import java.util.List;

public class ItemDaoImpl extends GenericDaoImpl<Item> implements ItemDao {
    public ItemDaoImpl(Class<Item> clazz) {
        super(clazz);
    }

    @Override
    public Item findById (long id){
        return (Item) getCurrentSession().createQuery("from Item where id=:id")
                .setParameter("id", id)
                .uniqueResult();
    }

    @Override
    public List<Item> findItems(int from, int to) { //find items in price range from - to
        return getCurrentSession().createQuery("select i from Item as i where i.price between "+from+" and "+to)
                .list();
    }

    @Override
    public Long countItems(int from, int to) { //count items in price range from - to
        return(Long) getCurrentSession().createQuery("select count (f.id) from Item f where f.price between "+from+" and "+to)
                .uniqueResult();
    }


}
