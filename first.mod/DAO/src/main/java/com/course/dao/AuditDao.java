package com.course.dao;

import com.course.dao.GenericDao;
import java.util.List;
import com.course.model.Audit;


public interface AuditDao extends GenericDao<Audit> {

    List<Audit> findByEvent(String eventType);
}
