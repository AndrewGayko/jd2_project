package com.course.dao;
import com.course.dao.GenericDao;
import com.course.model.Role;

public interface RoleDao extends GenericDao<Role> {
    Role getByName(String roleName);

    int deleteAll();

    int unbind(String commonRoleName); // Unbind permission from it's role

    void delete(); // delete roles without permissions

    }
