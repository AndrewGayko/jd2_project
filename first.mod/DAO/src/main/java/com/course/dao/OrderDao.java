package com.course.dao;

import com.course.model.Order;

import java.util.List;

public interface OrderDao extends GenericDao<Order> {
    List<Order> find();
}
