package com.course.dao.impl;

import com.course.dao.PermissionDao;
import com.course.model.Permission;

public class PermissionDaoImpl extends GenericDaoImpl<Permission> implements PermissionDao {
    public PermissionDaoImpl(Class<Permission> clazz) {
        super(clazz);
    }

    @Override
    public Permission findByName(String permissionName){
        return (Permission) getCurrentSession().createQuery("from Permission as p where p.name=:permissionname")
                .setParameter("permissionname", permissionName)
                .uniqueResult();
    }
}
