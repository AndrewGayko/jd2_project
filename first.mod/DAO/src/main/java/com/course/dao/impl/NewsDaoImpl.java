package com.course.dao.impl;

import com.course.dao.NewsDao;
import com.course.model.News;

import java.util.List;

public class NewsDaoImpl extends GenericDaoImpl<News> implements NewsDao {
    public NewsDaoImpl(Class<News> clazz) {
        super(clazz);
    }

    @Override
    public List<News> findByUserId(long userId){
    return getCurrentSession().createQuery("from News as n where n.user.id=:id")
            .setParameter("id", userId)
            .list();
}
}
