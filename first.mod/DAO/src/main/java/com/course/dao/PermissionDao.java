package com.course.dao;

import com.course.dao.GenericDao;
import com.course.model.Permission;

public interface PermissionDao extends GenericDao<Permission> {

    Permission findByName(String permissionName);
}
