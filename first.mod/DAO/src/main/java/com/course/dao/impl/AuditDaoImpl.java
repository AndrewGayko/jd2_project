package com.course.dao.impl;

import com.course.dao.AuditDao;
import com.course.model.Audit;

import java.util.List;

public class AuditDaoImpl extends GenericDaoImpl<Audit> implements AuditDao {
    public AuditDaoImpl(Class<Audit> clazz) {
        super(clazz);
    }

    @Override
    public List<Audit> findByEvent(String eventType) {
        return (List<Audit>) getCurrentSession().createQuery("from Audit as a where a.eventType=:eventtype")
                .setParameter("eventtype", eventType)
                .list();
    }

}
