package com.course.dao;

import com.course.dao.GenericDao;
import com.course.model.News;

import java.util.List;

public interface NewsDao extends GenericDao<News> {

    List<News> findByUserId(long id);
}
