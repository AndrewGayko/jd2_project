package com.course.dao;

import com.course.dao.GenericDao;
import com.course.model.Profile;

public interface ProfileDao extends GenericDao<Profile> {
}
