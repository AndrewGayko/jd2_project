package com.course.dao.impl;

import com.course.dao.ProfileDao;
import com.course.model.Profile;

public class ProfileDaoImpl extends GenericDaoImpl<Profile> implements ProfileDao {
    public ProfileDaoImpl(Class<Profile> clazz) {
        super(clazz);
    }
}
