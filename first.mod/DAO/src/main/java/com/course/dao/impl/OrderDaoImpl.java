package com.course.dao.impl;

import com.course.dao.OrderDao;
import com.course.model.Order;

import java.util.List;

public class OrderDaoImpl extends GenericDaoImpl<Order> implements OrderDao {
    public OrderDaoImpl(Class<Order> clazz) {
        super(clazz);
    }

    @Override
    public List<Order> find() {
        return getCurrentSession()
                .createQuery("from Order as o")
                .list();
    }
}
