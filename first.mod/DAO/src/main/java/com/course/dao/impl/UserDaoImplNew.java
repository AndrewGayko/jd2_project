package com.course.dao.impl;

import com.course.dao.UserDaoNew;
import com.course.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.Query;

public class UserDaoImplNew extends GenericDaoImpl<User> implements UserDaoNew {

    private static final Logger logger = LogManager.getLogger(UserDaoImplNew.class);

    public UserDaoImplNew(Class<User> clazz) {
        super(clazz);
    }

    //    @Override
//    public List<com.course.newmodel.User> findAll() {
//        String hql = "from User";
//        Query query = getCurrentSession().createQuery(hql);
//        return query.list();
//    }


    public User findByEmail(){
        String hql = "select * from User where email=?";
        Query query = getCurrentSession().createQuery(hql);
        return (User) query.uniqueResult();
    }

    public User findById(long id){
        return (User) getCurrentSession().createQuery("from User where id=:id")
                .setParameter("id", id)
                .uniqueResult();

    }
}
